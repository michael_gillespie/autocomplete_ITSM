what does it do?
autoComplete takes a .csv file of incidents from the ITSM and creates a 
.html that enables filtering/searching all types of incidents.

skills used:
> html/css
> JS
> ruby

requirements:
> web browser (tested in chrome)
> ruby
> ChartJS (https://www.chartjs.org/)

how to run:
1. get report from ITSM manager (saved as .xlsx)
2. save as .csv
3. run autoComplete.rb
