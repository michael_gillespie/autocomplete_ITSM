require 'csv'

report = Dir.glob('*.csv')[0]
t = CSV.read(report)

# cut columns down to 5
x = []
len = t.length
c = 0
while c < len do
  x << t[c][0..4]
  c += 1
end

# remove nil
x.each do |y|
  y.delete_if {|i| i == nil}
end

# add separator between columns
y = []
len = x.length
c = 0
while c < len do
  row_len = x[c].length
  row_len_c = 0
  y_temp = []
  while row_len_c < row_len do
    y_temp << x[c][row_len_c] + " > "
    y << y_temp
    row_len_c += 1
  end
  c += 1
end

# remove duplicates and column headers
y.uniq!
y.shift

# name and create output file
t = Time.now()
file_name = "autoComplete_#{t.day}-#{t.month}-#{t.year}.html"
file = File.open(file_name, 'w')

file.puts '<!DOCTYPE html>
<html>
<head>
<style>
h3 {
    background-color: lightgray;
    padding: .5em;
    font-style: bold;
    width: 26%;
}

#myInput {
    border-box: box-sizing;
    background-image: url(\'searchicon.png\');
    background-position: 14px 12px;
    background-repeat: no-repeat;
    font-size: 16px;
    padding: 14px 20px 12px 45px;
    border: none;
    border-bottom: 1px solid #ddd;
}

#myInput:focus {outline: 3px solid #ddd;}

.dropdown {
    position: relative;
    display: inline-block;
}

.dropdown-content {
    /*display: none;*/
    position: absolute;
    background-color: #f6f6f6;
    min-width: 230px;
    overflow: auto;
    border: 1px solid #ddd;
    z-index: 1;
}

.dropdown-content a {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
}

.dropdown a:hover {background-color: lightgray}

</style>
</head>
<body>

  <h2>Search/Filter Dropdown</h2>
  <h3>Use the search box to filter incident types:</h3>

  <div class="dropdown">
      <div id="myDropdown" class="dropdown-content">
          <input type="text" placeholder="Search.." id="myInput" onkeyup="filterFunction()">'

# create links for individual items
len = y.length
c = 0
while c < len do
  file.puts "<a href=\"##{y[c].join('')[0..-4]}\">#{y[c].join('')[0..-4]}</a>"
  c += 1
end

file.puts "</div>
</div>

  <script>
    function myFunction() {
      document.getElementById('myDropdown').classList.toggle('show');
    }
    
    function filterFunction() {
      var input, filter, ul, li, a, i;
      input = document.getElementById('myInput');
      filter = input.value.toUpperCase();
      div = document.getElementById('myDropdown');
      a = div.getElementsByTagName('a');
      for (i = 0; i < a.length; i++) {
        if (a[i].innerHTML.toUpperCase().indexOf(filter) > -1) {
          a[i].style.display = '';
        } else {
          a[i].style.display = 'none';
        }
      }
    }
  </script>
</body>
</html>"

file.close
